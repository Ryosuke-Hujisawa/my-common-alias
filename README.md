# my-common-alias

## Japanese correspondence

export LANG=ja_JP.UTF-8

## Move to mamp db folder


function mampdb() {
  cd "/Applications/MAMP/Library/bin/"
  ./mysql -u root -p
}

## Move to mamp folder

function mamp(){
cd "/Applications/MAMP/htdocs"
}

## make ailias 

function mk(){
vi ~/.bash_profile
}

## made ailias 

function md(){
source ~/.bashrc
}

## Write ssh config

function cf(){
vi ~./.ssh/config
}

## To bashrc

if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi